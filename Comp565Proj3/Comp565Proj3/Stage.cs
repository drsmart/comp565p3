﻿/*  
    Copyright (C) 2012 G. Michael Barnes
 
    The file Stage.cs is part of AGXNASKv4.

    AGXNASKv4 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;

namespace Comp565Proj3
{

    /// <summary>
    /// Stage.cs  is the environment / framework for AGXNASK.
    /// 
    /// Stage declares and initializes the common devices, Inspector pane,
    /// and user input options.  Stage attempts to "hide" the infrastructure of AGXNASK
    /// for the developer.  It's subclass Stage is where the program specific aspects are
    /// declared and created.
    /// 
    /// AGXNASK is a starter kit for Comp 565 assignments using XNA Game Studio 4.0
    /// and Visual Studio 2010.
    /// 
    /// See AGXNASKv4-doc.pdf file for class diagram and usage information. 
    /// 
    /// 1/25/2012   last  updated
    /// </summary>
    public class Stage : Game
    {
        protected const int range = 512;
        protected const int spacing = 150;  // x and z spaces between vertices in the terrain
        protected const int terrainSize = range * spacing;

        //random vars for spawning
        protected int randX, randZ;
        Random rand;

        // Graphics device
        protected GraphicsDeviceManager graphics;
        protected GraphicsDevice display;    // graphics context
        protected BasicEffect effect;        // default effects (shaders)
        protected SpriteBatch spriteBatch;   // for Trace's displayStrings
        protected BlendState blending, notBlending;

        // stage Models
        protected Model boundingSphere3D;    // a  bounding sphere model
        protected Model wayPoint3D;          // a way point marker -- for paths.
        protected bool drawBoundingSpheres = false;
        protected bool fog = false;
        protected bool fixedStepRendering = true;     // 60 updates / second

        // variables required for use with Inspector
        protected SpriteFont inspectorFont;

        // Projection values
        protected Matrix projection;
        //protected float fov = (float)Math.PI / 4;
        protected float hither = 5.0f, yon = terrainSize / 5.0f, farYon = terrainSize * 1.3f;
        protected float fogStart = 4000;
        protected float fogEnd = 10000;
        protected bool yonFlag = true;

        // User event state
        protected GamePadState oldGamePadState;
        protected GamePadState currentGamePadState;
        protected KeyboardState oldKeyboardState;
        protected KeyboardState currentKeyboardState;

        // Lights
        protected Vector3 lightDirection, ambientColor, diffuseColor;

        // Cameras
        protected int cameraIndex = 0;

        protected Terrain terrain;

        // Screen display information variables
        protected double fpsSecond;
        protected int draws, updates;
        Camera topDownCamera;

        //Viewports for split screen
        protected Viewport defaultViewport;
        protected Viewport sceneViewport;     // top and bottom "windows"
        protected Matrix sceneProjection, inspectorProjection;
        Viewport leftViewport;
        Viewport rightViewport;

        //Player data
        Model playerModel;
        Matrix[] playerTransforms;

        Player player;

        //Network stuff
        NetworkSession networkSession;
        AvailableNetworkSessionCollection availableSessions;
        PacketReader packetReader;
        PacketWriter packetWriter;
        int selectedSessionIndex;


        public Stage()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            graphics.SynchronizeWithVerticalRetrace = false;  // allow faster FPS

            // Directional light values
            lightDirection = Vector3.Normalize(new Vector3(-1.0f, -1.0f, -1.0f));
            ambientColor = new Vector3(0.4f, 0.4f, 0.4f);
            diffuseColor = new Vector3(0.2f, 0.2f, 0.2f);

            IsMouseVisible = true;  // make mouse cursor visible

            //Set Up network 
            packetWriter = new PacketWriter();
            packetReader = new PacketReader();

            Components.Add(new GamerServicesComponent(this));
            SignedInGamer.SignedIn += new EventHandler<SignedInEventArgs>(SignedInGamer_SignedIn);
        }

        // Properties

        public Vector3 AmbientLight
        {
            get { return ambientColor; }
        }

        public Model BoundingSphere3D
        {
            get { return boundingSphere3D; }
        }

        public Vector3 DiffuseLight
        {
            get { return diffuseColor; }
        }

        public GraphicsDevice Display
        {
            get { return display; }
        }

        public float FarYon
        {
            get { return farYon; }
        }

        public bool FixedStepRendering
        {
            get { return fixedStepRendering; }
            set
            {
                fixedStepRendering = value;
                IsFixedTimeStep = fixedStepRendering;
            }
        }

        public bool Fog
        {
            get { return fog; }
            set { fog = value; }
        }

        public float FogStart
        {
            get { return fogStart; }
        }

        public float FogEnd
        {
            get { return fogEnd; }
        }

        public Vector3 LightDirection
        {
            get { return lightDirection; }
        }

        public Matrix Projection
        {
            get { return projection; }
        }

        public int Range
        {
            get { return range; }
        }

        public BasicEffect SceneEffect
        {
            get { return effect; }
        }

        public int Spacing
        {
            get { return spacing; }
        }

        public Matrix View
        {
            //get { return currentCamera.ViewMatrix; }
            get
            {
                return topDownCamera.ViewMatrix;
                //return player.View;
            }
        }

        public Model WayPoint3D
        {
            get { return wayPoint3D; }
        }

        public bool YonFlag
        {
            get { return yonFlag; }
            set
            {
                yonFlag = value;
                if (yonFlag) setProjection(yon);
                else setProjection(farYon);
            }
        }

        // Methods

        /// <summary>
        /// Make sure that aMovableModel3D does not move off the terain.
        /// Called from MovableModel3D.Update()
        /// The Y dimension is not constrained -- code commented out.
        /// </summary>
        /// <param name="aName"> </param>
        /// <param name="newLocation"></param>
        /// <returns>true iff newLocation is within range</returns>
        public bool withinRange(Vector3 newLocation)
        {
            if (newLocation.X < spacing || newLocation.X > (terrainSize - 2 * spacing) ||
               newLocation.Z < spacing || newLocation.Z > (terrainSize - 2 * spacing))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected void setProjection(float yonValue)
        {
            projection = Matrix.CreatePerspectiveFieldOfView(GameConstants.Fovy,
            graphics.GraphicsDevice.Viewport.AspectRatio, hither, yonValue);
        }

        /// <summary>
        /// Changing camera view for Agents will always set YonFlag false
        /// and provide a clipped view.
        /// </summary>
        public void nextCamera()
        {
            topDownCamera.updateViewMatrix();
            //cameraIndex = (cameraIndex + 1) % camera.Count;
            //currentCamera = camera[cameraIndex];
            //// set the appropriate projection matrix
            //YonFlag = false;
            setProjection(farYon);
        }

        public void setBlendingState(bool state)
        {
            if (state) display.BlendState = blending;
            else display.BlendState = notBlending;
        }

        // Overridden Game class methods. 

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
        }

        public int RandomX()
        {
            rand = new Random();
            randX = rand.Next(0, 76800);
           // return randX;
            return 00;
        }

        public int RandomZ()
        {
            rand = new Random();
            randZ = rand.Next(0, 76800);
            //return randZ;
            return 00;
        }

        /// <summary>
        /// Set GraphicDevice display and rendering BasicEffect effect.  
        /// Create SpriteBatch, font, and font positions.
        /// Creates the traceViewport to display information and the sceneViewport
        /// to render the environment.
        /// Create and add all DrawableGameComponents and Cameras.
        /// </summary>
        protected override void LoadContent()
        {
            display = graphics.GraphicsDevice;
            effect = new BasicEffect(display);
            // Set up Inspector display
            spriteBatch = new SpriteBatch(display);      // Create a new SpriteBatch
            inspectorFont = Content.Load<SpriteFont>("Fonts/Courier New");  // load font
            boundingSphere3D = Content.Load<Model>("Models/castle");

            // create Inspector display
            // set blending for bounding sphere drawing
            blending = new BlendState();
            blending.ColorSourceBlend = Blend.SourceAlpha;
            blending.ColorDestinationBlend = Blend.InverseSourceAlpha;
            blending.ColorBlendFunction = BlendFunction.Add;
            notBlending = new BlendState();
            notBlending = display.BlendState;

            // Create and add stage components
            terrain = new Terrain(this, "terrain", "Textures/heightTexture1", "Textures/colorTexture1");

            // Create a top-down "Whole stage" camera view, make it first camera in collection.
            topDownCamera = new Camera(this, Camera.CameraEnum.TopDownCamera);

            //Load Player model data
            setProjection(farYon);
            playerModel = Content.Load<Model>("Models/castle");
            playerTransforms = SetupEffectDefaults(playerModel);

            // Set up viewports for split screen
            defaultViewport = GraphicsDevice.Viewport;
            leftViewport = defaultViewport;
            rightViewport = defaultViewport;

            //leftViewport.Width = leftViewport.Width / 2;
            //rightViewport.Width = rightViewport.Width / 2;
            //rightViewport.X = leftViewport.Width + 1;

            sceneProjection = Matrix.CreatePerspectiveFieldOfView((float)Math.PI / 4.0f, defaultViewport.Width / defaultViewport.Height, 1.0f, 10000.0f);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Uses an Inspector to display update and display information to player.
        /// All user input that affects rendering of the stage is processed either
        /// from the gamepad or keyboard.
        /// See Player.Update(...) for handling of user events that affect the player.
        /// The current camera's place is updated after all other GameComponents have 
        /// been updated.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (!Guide.IsVisible)
            {
                if (Gamer.SignedInGamers.Count == 0)
                    Guide.ShowSignIn(1, false);
                foreach (SignedInGamer signedIn in SignedInGamer.SignedInGamers)
                {
                    Player player = signedIn.Tag as Player;
                    oldKeyboardState = player.OldKeyboardState;
                    currentKeyboardState = Keyboard.GetState(signedIn.PlayerIndex);
                    player.CurrentKeyboardState = currentKeyboardState;

                    if (networkSession != null)
                    {
                        if (networkSession.SessionState == NetworkSessionState.Lobby)
                            HandleLobbyInput();
                        else
                            HandleGameplayInput(player, gameTime);
                    }
                    else if (availableSessions != null)
                    {
                        HandleAvailableSessionInput();
                    }
                    else
                    {
                        HandleTitleScreenInput();
                    }

                    player.OldKeyboardState = currentKeyboardState;

                    //HandlePlayerInput(player, gameTime);
                }
            }

            topDownCamera.updateViewMatrix();
            base.Update(gameTime);
        }
        /// <summary>
        /// Draws information in the display viewport.
        /// Resets the GraphicsDevice's context and makes the sceneViewport active.
        /// Has Game invoke all DrawableGameComponents Draw(GameTime).
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>


        //protected void DrawGame(GameTime gameTime)
        //{
        //    draws++;
        //    display.Viewport = defaultViewport; //sceneViewport;
        //    display.Clear(Color.CornflowerBlue);
        //    // need to restore state changed by spriteBatch
        //    GraphicsDevice.BlendState = BlendState.Opaque;
        //    GraphicsDevice.DepthStencilState = DepthStencilState.Default;
        //    GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
        //    // draw objects in stage 
        //    display.Viewport = sceneViewport;
        //    display.RasterizerState = RasterizerState.CullNone;
        //    //base.Draw(gameTime);  // draw all GameComponents and DrawableGameComponents
        //}

        protected void SignedInGamer_SignedIn(object sender, SignedInEventArgs e)
        {
            e.Gamer.Tag = new Player(this, new Vector3(RandomX(), terrain.surfaceHeight(randX, randZ), RandomZ()));
        }

        private void UpdateInput(Player player, GameTime gameTime)
        {

            foreach (LocalNetworkGamer gamer in networkSession.LocalGamers)
            {
                ReceiveNetworkData(gamer, gameTime);

                if (currentGamePadState.IsConnected)
                {
                    if (IsButtonPressed(Buttons.A))
                        nextCamera();
                    else if (IsButtonPressed(Buttons.B))
                        Fog = !Fog;
                    else if (IsButtonPressed(Buttons.X))
                        Exit();
                    else if (IsButtonPressed(Buttons.Y))
                        FixedStepRendering = !FixedStepRendering;

                    oldGamePadState = currentGamePadState;
                }
                else
                {
                    if (IsKeyPressed(Keys.Escape))
                        Exit();
                    //else if (IsKeyPressed(Keys.B))
                    //    DrawBoundingSpheres = !DrawBoundingSpheres;
                    //else if (IsKeyPressed(Keys.C))
                    //    nextCamera();
                    //else if (IsKeyPressed(Keys.F))
                    //    Fog = !Fog;
                    //else if (IsKeyPressed(Keys.H))
                    //{
                    //    inspector.ShowHelp = !inspector.ShowHelp;
                    //    inspector.ShowMatrices = false;
                    //}
                    //else if (IsKeyPressed(Keys.I))
                    //    inspector.showInfo();
                    //else if (IsKeyPressed(Keys.M))
                    //{
                    //    inspector.ShowMatrices = !inspector.ShowMatrices;
                    //    inspector.ShowHelp = false;
                    //}
                    else if (IsKeyPressed(Keys.T))
                        FixedStepRendering = !FixedStepRendering;
                    else if (IsKeyPressed(Keys.Y))
                        YonFlag = !YonFlag;

                    oldKeyboardState = currentKeyboardState;

                }

                packetWriter.Write(player.Position);
                packetWriter.Write(player.Yaw);

                gamer.SendData(packetWriter, SendDataOptions.None);
            }
        }

        #region HandleInput

        private void HandleGameplayInput(Player player, GameTime gameTime)
        {
            if (IsButtonPressed(Buttons.Back))
                this.Exit();
            if (IsKeyPressed(Keys.Escape))
                this.Exit();

            UpdateInput(player, gameTime);
            player.Update(gameTime);
            networkSession.Update();
        }

        private void HandleTitleScreenInput()
        {
            if (IsButtonPressed(Buttons.A) || IsKeyPressed(Keys.A))
                CreateSession();
            else if (IsButtonPressed(Buttons.X) || IsKeyPressed(Keys.F))
            {
                availableSessions = NetworkSession.Find(NetworkSessionType.SystemLink, 1, null);
                selectedSessionIndex = 0;
            }
            else if (IsButtonPressed(Buttons.B) || IsKeyPressed(Keys.B))
            {
                Exit();
            }
        }

        private void HandleLobbyInput()
        {
            if (IsButtonPressed(Buttons.A) || IsKeyPressed(Keys.A))
            {
                foreach (LocalNetworkGamer gamer in networkSession.LocalGamers)
                    gamer.IsReady = true;
            }

            if (IsButtonPressed(Buttons.B) || IsKeyPressed(Keys.B))
            {
                networkSession = null;
                availableSessions = null;
            }

            if (networkSession.IsHost)
            {
                if (networkSession.IsEveryoneReady)
                    networkSession.StartGame();  
            }

            networkSession.Update();
        }

        private void HandleAvailableSessionInput()
        {
            if (IsButtonPressed(Buttons.A) || IsKeyPressed(Keys.A))
            {
                if (availableSessions.Count > 0)
                {
                    networkSession = NetworkSession.Join(availableSessions[selectedSessionIndex]);
                    HookSessionEvents();
                    availableSessions.Dispose();
                    availableSessions = null;
                }
            }
            else if (IsButtonPressed(Buttons.DPadUp) || IsKeyPressed(Keys.Up))
            {
                if (selectedSessionIndex > 0)
                    selectedSessionIndex++;
            }
            else if (IsButtonPressed(Buttons.DPadDown) || IsKeyPressed(Keys.Up))
            {
                if (selectedSessionIndex < availableSessions.Count - 1)
                    selectedSessionIndex++;
            }
            else if (IsButtonPressed(Buttons.B) || IsKeyPressed(Keys.B))
            {
                availableSessions.Dispose();
                availableSessions = null;
            }
        }

        #endregion

        private Matrix[] SetupEffectDefaults(Model model)
        {
            Matrix[] absTransforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(absTransforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.Projection = Projection;
                }
            }
            return absTransforms;
        }


        #region Inputh Helpers
        private Boolean IsButtonPressed(Buttons b)
        {
            switch (b)
            {
                case Buttons.A:
                    return (currentGamePadState.Buttons.A == ButtonState.Pressed && oldGamePadState.Buttons.A != ButtonState.Pressed);
                
                case Buttons.B:
                    return (currentGamePadState.Buttons.B == ButtonState.Pressed && oldGamePadState.Buttons.B != ButtonState.Pressed);
                
                case Buttons.Y:
                    return (currentGamePadState.Buttons.Y == ButtonState.Pressed && oldGamePadState.Buttons.Y != ButtonState.Pressed);
                
                case Buttons.X:
                    return (currentGamePadState.Buttons.X == ButtonState.Pressed);
                
                case Buttons.Back:
                    return (currentGamePadState.Buttons.Back == ButtonState.Pressed &&
                        oldGamePadState.Buttons.Back == ButtonState.Released);
                
                case Buttons.DPadDown:
                    return (currentGamePadState.DPad.Down == ButtonState.Pressed &&
                        oldGamePadState.DPad.Down == ButtonState.Released);
                
                case Buttons.DPadUp:
                    return (currentGamePadState.DPad.Up == ButtonState.Pressed &&
                        oldGamePadState.DPad.Down == ButtonState.Released);
            }

            return false;
        }

        private Boolean IsKeyPressed(Keys key)
        {
            return currentKeyboardState.IsKeyDown(key) && !oldKeyboardState.IsKeyDown(key);
        }

        #endregion

        public float SurfaceHeight(float x, float z)
        {
            return terrain.surfaceHeight(x, z);
        }

        #region Session Creation
        private void CreateSession()
        {
            networkSession = NetworkSession.Create(NetworkSessionType.SystemLink, 1, 8, 2, null);
            networkSession.AllowHostMigration = true;
            networkSession.AllowJoinInProgress = true;
            HookSessionEvents();
        }

        private void HookSessionEvents()
        {
            networkSession.GamerJoined += new EventHandler<GamerJoinedEventArgs>(networkSession_GamerJoined);
        }

        private void networkSession_GamerJoined(object sender, GamerJoinedEventArgs e)
        {
            if (!e.Gamer.IsLocal)
            {
                e.Gamer.Tag = new Player(this, new Vector3(RandomX(), terrain.surfaceHeight(randX, randZ), RandomZ()));
            }
            else
            {
                e.Gamer.Tag = GetPlayer(e.Gamer.Gamertag);
            }
        }

        private Player GetPlayer(String gamerTag)
        {
            foreach (SignedInGamer gamer in SignedInGamer.SignedInGamers)
            {
                if (gamer.Gamertag == gamerTag)
                    return gamer.Tag as Player;
            }

            return new Player(this, new Vector3(RandomX(), terrain.surfaceHeight(randX, randZ), RandomZ()));
        }
        #endregion

        #region Draw Methods

        protected override void Draw(GameTime gameTime)
        {
            if (networkSession != null)
            {
                if (networkSession.SessionState == NetworkSessionState.Lobby)
                    DrawLobby();
                else
                    DrawGameplay(gameTime);
            }
            else if (availableSessions != null)
            {
                DrawAvailableSessions();
            }
            else
            {
                DrawTitleScreen();
            }

            base.Draw(gameTime);
        }

        private void DrawGameplay(GameTime gameTime)
        {
            GraphicsDevice.Viewport = defaultViewport;
            GraphicsDevice.Clear(Color.CornflowerBlue);

            Player player;

            if (networkSession != null)
            {
                foreach (NetworkGamer netGamer in networkSession.AllGamers)
                {
                    player = netGamer.Tag as Player;
                    DrawPlayer(player, defaultViewport);
                }
                terrain.Draw();
            }
        }

        private void DrawTitleScreen()
        {
            display.Clear(Color.Beige);
            string message = "";

            if (SignedInGamer.SignedInGamers.Count == 0)
            {
                message = "No Profile signed in";
            }
            else
            {
                message += "Press A to create new session";
            }

            spriteBatch.Begin();
            spriteBatch.DrawString(inspectorFont, message, new Vector2(101, 201), Color.Black);
            spriteBatch.DrawString(inspectorFont, message, new Vector2(100, 200), Color.White);
            spriteBatch.End();
        }

        private void DrawAvailableSessions()
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();
            float y = 100;

            spriteBatch.DrawString(inspectorFont, "Available sessions (A = join, B = Back)", new Vector2(101, y + 1), Color.Black);
            spriteBatch.DrawString(inspectorFont, "Available sessions (A = join, B = Back)", new Vector2(101, y), Color.White);

            y += inspectorFont.LineSpacing * 2;

            int selectedSessionIndex = 0;

            for (int sessionIndex = 0; sessionIndex < availableSessions.Count; sessionIndex++)
            {
                Color color = Color.Black;

                if (sessionIndex == selectedSessionIndex)
                    color = Color.Yellow;

                spriteBatch.DrawString(inspectorFont, availableSessions[sessionIndex].HostGamertag, new Vector2(100, y), color);
                y += inspectorFont.LineSpacing;
            }

            spriteBatch.End();
        }

        private void DrawLobby()
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            float y = 100;
            spriteBatch.DrawString(inspectorFont, "Lobby (A = ready, B = leave)", new Vector2(101, y + 1), Color.Black);
            spriteBatch.DrawString(inspectorFont, "Lobby (A = ready, B = leave)", new Vector2(101, y), Color.White);

            y += inspectorFont.LineSpacing * 2;

            foreach (NetworkGamer gamer in networkSession.AllGamers)
            {
                string text = gamer.Gamertag;
                Player player = gamer.Tag as Player;

                if (player.picture == null)
                {
                    GamerProfile profile = gamer.GetProfile();
                    System.IO.Stream stream = profile.GetGamerPicture();
                    player.picture = Texture2D.FromStream(GraphicsDevice, stream);
                }

                if (gamer.IsReady)
                    text += " - ready!";

                spriteBatch.Draw(player.picture, new Vector2(100, y), Color.White);
                spriteBatch.DrawString(inspectorFont, text, new Vector2(170, y), Color.White);
                y += inspectorFont.LineSpacing + 64;
            }
            spriteBatch.End();
        }

        private void DrawPlayer(Player player, Viewport viewport)
        {
            graphics.GraphicsDevice.Viewport = viewport;

            Matrix playerTransforms = player.Orientation;
            DrawModel(playerModel, playerTransforms, this.playerTransforms, player.View);
        }

        private void DrawModel(Model model, Matrix transforms, Matrix[] boneTransforms, Matrix view)
        {
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.World = boneTransforms[mesh.ParentBone.Index] * transforms;
                    effect.View = View;
                }
                mesh.Draw();
            }
        }
        #endregion


        private void ReceiveNetworkData(LocalNetworkGamer gamer, GameTime gameTime)
        {
            while (gamer.IsDataAvailable)
            {
                NetworkGamer sender;
                gamer.ReceiveData(packetReader, out sender);

                if (!sender.IsLocal)
                {
                    Player player = sender.Tag as Player;
                    player.Position = packetReader.ReadVector3();
                    player.Yaw = (float)packetReader.ReadDouble();
                    player.Update(gameTime);
                }
            }
        }
    }
}
