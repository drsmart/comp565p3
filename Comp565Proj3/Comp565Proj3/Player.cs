/*  
    Copyright (C) 2012 G. Michael Barnes
 
    The file Player.cs is part of AGXNASKv4.

    AGXNASKv4 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Comp565Proj3
{

    /// <summary>
    /// Represents the user / player interacting with the stage. 
    /// The Update(Gametime) handles both user keyboard and gamepad controller input.
    /// If there is a gamepad attached the keyboard inputs are not processed.
    /// 
    /// 1/25/2012 last changed
    /// </summary>

    public class Player
    {
        private GamePadState oldGamePadState;
        private KeyboardState oldKeyboardState;
        private KeyboardState currentKeyboardState;
        
        private float rotate;
        private float angle;
        private Matrix orientation;
        
        private Camera firstPersonCamera;
        public Camera thirdPersonCamera;
        private Camera aboveCamera;
        private Camera currentCamera;

        private Camera[] cameras;
        private int cameraIndex;

        private Vector3 position;

        private bool isCollidable;
        private string playerName;

        private int step;
        private int stepSize;
        private float yaw;
        internal Texture2D picture;

        private Stage stage;

        public KeyboardState OldKeyboardState
        {
            get { return oldKeyboardState; }
            set { oldKeyboardState = value; }
        }

        public Matrix Orientation
        {
            get { return orientation; }
        }

        public Vector3 Position
        {
            get { return orientation.Translation; }
            set { orientation.Translation = value; }
        }

        public float Yaw
        {
            get { return yaw; }
            set { yaw = value; }
        }

        public Vector3 Forward
        {
            get { return orientation.Forward; }
        }

        public KeyboardState CurrentKeyboardState
        {
            set { currentKeyboardState = value; }
        }

        public Matrix View
        {
            get { return currentCamera.ViewMatrix; }
        }

        public Player(Stage theStage, Vector3 pos)
        {  
            position = pos;
            rotate = 0;
            angle = 0.05f;
            stepSize = 48;
            orientation = Matrix.Identity;
            orientation *= Matrix.CreateRotationY((float)Math.PI / 2);
            orientation *= Matrix.CreateTranslation(position);
            stage = theStage;

            firstPersonCamera = new Camera(theStage, this, Camera.CameraEnum.FirstCamera);
            firstPersonCamera.Name = "First";

            thirdPersonCamera = new Camera(theStage, this, Camera.CameraEnum.FollowCamera);
            thirdPersonCamera.Name = "Follow";

            aboveCamera = new Camera(theStage, this, Camera.CameraEnum.AboveCamera);
            aboveCamera.Name = "Above";

            currentCamera = firstPersonCamera;
            cameras = new Camera[] {firstPersonCamera, thirdPersonCamera, aboveCamera};
            cameraIndex = 0;
        }

        /// <summary>
        /// Handle player input that affects the player.
        /// See Stage.Update(...) for handling user input that affects
        /// how the stage is rendered.
        /// First check if gamepad is connected, if true use gamepad
        /// otherwise assume and use keyboard.
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);

            if (gamePadState.IsConnected)
            {
                //if (gamePadState.Buttons.X == ButtonState.Pressed)
                //    stage.Exit();
                if (gamePadState.Buttons.A == ButtonState.Pressed && oldGamePadState.Buttons.A != ButtonState.Pressed)
                    NextCamera();
                //else if (gamePadState.Buttons.B == ButtonState.Pressed && oldGamePadState.Buttons.B != ButtonState.Pressed)
                //    stage.Fog = !stage.Fog;
                //else if (gamePadState.Buttons.RightShoulder == ButtonState.Pressed && oldGamePadState.Buttons.RightShoulder != ButtonState.Pressed)
                //    stage.FixedStepRendering = !stage.FixedStepRendering;

                // allow more than one gamePadState to be pressed
                if (gamePadState.ThumbSticks.Left.Y > 0.0)
                    step++;
                if (gamePadState.ThumbSticks.Left.Y > 0.0 && gamePadState.Buttons.LeftStick == ButtonState.Pressed)
                    step *= 3;
                if (gamePadState.ThumbSticks.Left.Y < 0.0)
                    step--;

                if (gamePadState.ThumbSticks.Right.X > 0.0)
                    rotate = gamePadState.ThumbSticks.Right.X;

                if (gamePadState.ThumbSticks.Right.X < 0.0)
                    rotate = gamePadState.ThumbSticks.Right.X;
                

                oldGamePadState = gamePadState;
            }
            else
            {
                // no gamepad assume use of keyboard
                currentKeyboardState = Keyboard.GetState();

                //if (currentKeyboardState.IsKeyDown(Keys.R) && !oldKeyboardState.IsKeyDown(Keys.R))
                //    agentObject.Orientation = Orientation;

                // allow more than one currentKeyboardState to be pressed
                if (currentKeyboardState.IsKeyDown(Keys.Up))
                    step++;
                if (currentKeyboardState.IsKeyDown(Keys.Down))
                    step--;
                if (currentKeyboardState.IsKeyDown(Keys.Left))
                    rotate++;
                if (currentKeyboardState.IsKeyDown(Keys.Right))
                    rotate--;

                oldKeyboardState = currentKeyboardState;    // Update saved state.
            }

            yaw = (rotate * angle)*-1;
          
            UpdateOrientation();
            thirdPersonCamera.updateViewMatrix();
            firstPersonCamera.updateViewMatrix();
            aboveCamera.updateViewMatrix();
            rotate = step = 0;
        }

        private void UpdateOrientation()
        {
            Vector3 startPos;
            Vector3 stopPos;
            startPos = stopPos = Position;
            stopPos += ((step * stepSize) * Forward);
            orientation *= Matrix.CreateTranslation(-1 * Position);
            orientation *= Matrix.CreateRotationY(yaw);
            
            stopPos.Y = stage.SurfaceHeight(stopPos.X, stopPos.Z) + 500;
          //  if (stage.withinRange(stopPos))
                orientation *= Matrix.CreateTranslation(stopPos);
            //else
              //  orientation *= Matrix.CreateTranslation(startPos);
        }

        private void NextCamera()
        {
            cameraIndex = (++cameraIndex) % cameras.Length;
            currentCamera = cameras[cameraIndex];
        }
    }
}
